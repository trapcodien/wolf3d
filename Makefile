# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
##   Updated: 2014/05/16 20:56:21 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = wolf3d
LIBS = -lft -lm -lmlx -lXext -lX11
FTLIBS = libft.a

LIB_DIR = libft
SOURCES_DIR = srcs
INCLUDES_DIR = includes

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra -pedantic
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -pedantic -std=c89
endif

ifeq ($(STRICT), 1)
	FLAGS += -fstack-protector-all -Wshadow -Wunreachable-code \
			  -Wstack-protector -pedantic-errors -O0 -W -Wundef -fno-common \
			  -Wfatal-errors -Wstrict-prototypes -Wmissing-prototypes \
			  -Wwrite-strings -Wunknown-pragmas -Wdeclaration-after-statement \
			  -Wold-style-definition -Wmissing-field-initializers \
			  -Wpointer-arith -Wnested-externs -Wstrict-overflow=5 -fno-common \
			  -Wno-missing-field-initializers -Wswitch-default -Wswitch-enum \
			  -Wbad-function-cast -Wredundant-decls -fno-omit-frame-pointer \
			  -Wfloat-equal
endif

CFLAGS =  $(FLAGS) -I $(INCLUDES_DIR) -I ./$(LIB_DIR)/includes
LDFLAGS = -L /usr/X11/lib -L $(LIB_DIR) $(LIBS)

DEPENDENCIES = \
			   $(INCLUDES_DIR)/wolf.h \
			   $(INCLUDES_DIR)/struct.h

SOURCES = \
		  $(SOURCES_DIR)/init.c \
		  $(SOURCES_DIR)/wolf.c \
		  $(SOURCES_DIR)/ft_parser.c \
		  $(SOURCES_DIR)/ft_draw.c \
		  $(SOURCES_DIR)/ft_calc.c \
		  $(SOURCES_DIR)/ft_move.c \
		  $(SOURCES_DIR)/ft_image.c

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS) lib
	@echo Creating $(NAME)...
	@$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

lib:
	@make $(FTLIBS) -C $(LIB_DIR)

test:
	@$(MAKE)
	@echo
	@echo ------- Program Start -------
	@echo
	@./$(NAME)

clean:
	@make clean -C $(LIB_DIR)
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: clean
	@make fclean -C $(LIB_DIR)
	@rm -f $(NAME)
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: clean fclean re all lib test

