/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_data.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/02 01:26:21 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 03:34:05 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_DATA_H
# define LIBFT_DATA_H

# include "ft_split.h"
# include "ft_list.h"
# include "ft_dlist.h"
# include "ft_btree.h"
# include "ft_htable.h"

#endif
