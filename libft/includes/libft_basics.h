/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_basics.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/02 01:18:22 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 03:34:03 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_BASICS_H
# define LIBFT_BASICS_H

# include "config.h"
# include "ft_standard_helpers.h"
# include "ft_number.h"
# include "ft_char.h"
# include "ft_string.h"
# include "ft_memory.h"

#endif
