/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 05:48:25 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 01:55:37 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	move(int dir, t_env *e)
{
	if (dir == KEY_UP)
		move_up(e);
	if (dir == KEY_DOWN)
		move_down(e);
	if (dir == KEY_LEFT)
		move_left(e);
	if (dir == KEY_RIGHT)
		move_right(e);
	if (dir == UP_LEFT || dir == UP_RIGHT)
		move_up(e);
	if (dir == DOWN_LEFT || dir == DOWN_RIGHT)
		move_down(e);
	if (dir == UP_LEFT || dir == DOWN_LEFT)
		move_left(e);
	if (dir == UP_RIGHT || dir == DOWN_RIGHT)
		move_right(e);
	ft_draw(e);
}

void	move_up(t_env *e)
{
	int		col_x;
	int		col_y;

	col_x = (int)(e->player.posx + e->player.dirx);
	col_y = (int)(e->player.posy + e->player.diry);
	if (e->map[col_x][(int)e->player.posy] == 0)
		e->player.posx += e->player.dirx / 4;
	if (e->map[(int)e->player.posx][col_y] == 0)
		e->player.posy += e->player.diry / 4;
}

void	move_down(t_env *e)
{
	int		col_x;
	int		col_y;

	col_x = (int)(e->player.posx - e->player.dirx);
	col_y = (int)(e->player.posy - e->player.diry);
	if (e->map[col_x][(int)e->player.posy] == 0)
		e->player.posx -= e->player.dirx / 4;
	if (e->map[(int)e->player.posx][col_y] == 0)
		e->player.posy -= e->player.diry / 4;
}

void	move_left(t_env *e)
{
	double	old;
	double	dirx;
	double	planex;

	old = e->player.dirx;
	dirx = e->player.dirx * cos(-RSPEED) - e->player.diry * sin(-RSPEED);
	e->player.dirx = dirx;
	e->player.diry = old * sin(-RSPEED) + e->player.diry * cos(-RSPEED);
	old = e->player.planex;
	planex = e->player.planex * cos(-RSPEED) - e->player.planey * sin(-RSPEED);
	e->player.planex = planex;
	e->player.planey = old * sin(-RSPEED) + e->player.planey * cos(-RSPEED);
}

void	move_right(t_env *e)
{
	double	old;
	double	dirx;
	double	planex;

	old = e->player.dirx;
	dirx = e->player.dirx * cos(RSPEED) - e->player.diry * sin(RSPEED);
	e->player.dirx = dirx;
	e->player.diry = old * sin(RSPEED) + e->player.diry * cos(RSPEED);
	old = e->player.planex;
	planex = e->player.planex * cos(RSPEED) - e->player.planey * sin(RSPEED);
	e->player.planex = planex;
	e->player.planey = old * sin(RSPEED) + e->player.planey * cos(RSPEED);
}
