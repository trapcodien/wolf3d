/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 03:23:22 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 03:31:49 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	init_player(t_env *e)
{
	e->player.posx = 1.1;
	e->player.posy = 1.1;
	e->player.dirx = 1.0;
	e->player.diry = 0.0;
	e->player.planex = 0.0;
	e->player.planey = 1.0;
}

void	ft_exit(const char *str)
{
	ft_putendl(str);
	exit(1);
}
