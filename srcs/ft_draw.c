/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 06:33:24 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 03:18:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static t_uint	get_color(t_cast *rc)
{
	t_uint		color;

	color = 0;
	if (rc->side == 1 && rc->mapy < rc->rposy)
		color = NORTH;
	else if (rc->side == 1)
		color = SOUTH;
	else if (rc->side == 0 && rc->mapx < rc->rposx)
		color = EAST;
	else if (rc->side == 0)
		color = WEST;
	return (color);
}

void			ft_put_slice(t_cast *rc, t_env *e, int x, t_uint color)
{
	int			height;
	int			start;
	int			end;
	int			i;

	height = abs(WIN_HEIGHT / rc->wdist);
	start = -height / 2 + WIN_HEIGHT / 2;
	if (start < 0)
		start = 0;
	end = height / 2 + WIN_HEIGHT / 2;
	if (end >= WIN_HEIGHT)
		end = WIN_HEIGHT - 1;
	i = WIN_HEIGHT;
	while (--i > end + 1)
		ft_img_pixel_put(e->screen, x, i, FLOOR);
	i = -1;
	while (++i < start - 1)
		ft_img_pixel_put(e->screen, x, i, CEIL);
	while (end >= start)
	{
		ft_img_pixel_put(e->screen, x, end, color);
		end--;
	}
}

void			ft_draw(t_env *e)
{
	int			x;
	t_cast		rc;
	t_uint		color;

	ft_img_reset(e->screen);
	x = 0;
	color = 0;
	while (x < WIN_WIDTH)
	{
		ft_init_raycast(&rc, e, x);
		ft_get_step(&rc);
		ft_dda(&rc, e);
		ft_get_wdist(&rc);
		if (!color)
			color = get_color(&rc);
		if ((ISWALL(color)) && (color == ((get_color(&rc)))))
			ft_put_slice(&rc, e, x, color);
		else
			ft_put_slice(&rc, e, x, COLOR(0, 0, 0));
		color = get_color(&rc);
		x++;
	}
	ft_img_display(e->screen);
}
