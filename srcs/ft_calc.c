/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 06:14:29 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 00:12:15 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	ft_get_step(t_cast *rc)
{
	if (rc->rdirx < 0)
	{
		rc->stepx = -1;
		rc->sdistx = (rc->rposx - rc->mapx) * rc->ddistx;
	}
	else
	{
		rc->stepx = 1;
		rc->sdistx = (rc->mapx + 1.0 - rc->rposx) * rc->ddistx;
	}
	if (rc->rdiry < 0)
	{
		rc->stepy = -1;
		rc->sdisty = (rc->rposy - rc->mapy) * rc->ddisty;
	}
	else
	{
		rc->stepy = 1;
		rc->sdisty = (rc->mapy + 1.0 - rc->rposy) * rc->ddisty;
	}
}

void	ft_dda(t_cast *rc, t_env *e)
{
	while (rc->hit == 0)
	{
		if (rc->sdistx < rc->sdisty)
		{
			rc->sdistx += rc->ddistx;
			rc->mapx += rc->stepx;
			rc->side = 0;
		}
		else
		{
			rc->sdisty += rc->ddisty;
			rc->mapy += rc->stepy;
			rc->side = 1;
		}
		if (e->map[rc->mapx][rc->mapy] > 0)
			rc->hit = 1;
	}
	(void)e;
}

void	ft_init_raycast(t_cast *rc, t_env *e, int x)
{
	rc->hit = 0;
	rc->camx = ((2 * x) / (double)WIN_WIDTH) - 1;
	rc->rposx = e->player.posx;
	rc->rposy = e->player.posy;
	rc->rdirx = e->player.dirx + (e->player.planex * rc->camx);
	rc->rdiry = e->player.diry + (e->player.planey * rc->camx);
	rc->mapx = (int)(rc->rposx);
	rc->mapy = (int)(rc->rposy);
	rc->ddistx = sqrt(1 + (pow(rc->rdiry, 2.0) / pow(rc->rdirx, 2.0)));
	rc->ddisty = sqrt(1 + (pow(rc->rdirx, 2.0) / pow(rc->rdiry, 2.0)));
}

void	ft_get_wdist(t_cast *rc)
{
	if (rc->side == 0)
		rc->wdist = fabs((rc->mapx - rc->rposx +
					(1 - rc->stepx) / 2) / rc->rdirx);
	else
		rc->wdist = fabs((rc->mapy - rc->rposy +
					(1 - rc->stepy) / 2) / rc->rdiry);
}
