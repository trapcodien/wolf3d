/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/16 20:35:27 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 03:34:18 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int		ft_keypress(int keycode, t_env *e)
{
	if (keycode == KEY_ESC)
	{
		ft_destroy_map(e->map, e->mapx);
		ft_img_destroy(e->screen);
		exit(0);
	}
	if (keycode == KEY_UP)
		e->player.move.up = 1;
	if (keycode == KEY_DOWN)
		e->player.move.down = 1;
	if (keycode == KEY_LEFT)
		e->player.move.left = 1;
	if (keycode == KEY_RIGHT)
		e->player.move.right = 1;
	return (0);
}

int		ft_keyrelease(int keycode, t_env *e)
{
	if (keycode == KEY_UP)
		e->player.move.up = 0;
	if (keycode == KEY_DOWN)
		e->player.move.down = 0;
	if (keycode == KEY_LEFT)
		e->player.move.left = 0;
	if (keycode == KEY_RIGHT)
		e->player.move.right = 0;
	return (0);
}

int		ft_expose_hook(t_env *e)
{
	ft_draw(e);
	return (0);
}

int		ft_loop(t_env *e)
{
	if (e->player.move.up && e->player.move.left)
		move(UP_LEFT, e);
	else if (e->player.move.up && e->player.move.right)
		move(UP_RIGHT, e);
	else if (e->player.move.down && e->player.move.left)
		move(DOWN_LEFT, e);
	else if (e->player.move.down && e->player.move.right)
		move(DOWN_RIGHT, e);
	else if (e->player.move.up)
		move(KEY_UP, e);
	else if (e->player.move.down)
		move(KEY_DOWN, e);
	else if (e->player.move.left)
		move(KEY_LEFT, e);
	else if (e->player.move.right)
		move(KEY_RIGHT, e);
	return (0);
}

int		main(int argc, char **argv)
{
	t_env	e;

	ft_bzero(&e, sizeof(t_env));
	if (argc == 2)
	{
		e.map = ft_parse_map(&e, argv[1], 0, 0);
		ft_check_map(&e);
		init_player(&e);
		e.mlx = mlx_init();
		e.width = WIN_WIDTH;
		e.height = WIN_HEIGHT;
		e.win = mlx_new_window(e.mlx, e.width, e.height, WIN_TITLE);
		e.screen = ft_img_create(&e, e.width, e.height);
		mlx_hook(e.win, KeyPress, KeyPressMask, &ft_keypress, &e);
		mlx_hook(e.win, KeyRelease, KeyReleaseMask, &ft_keyrelease, &e);
		mlx_expose_hook(e.win, ft_expose_hook, &e);
		mlx_loop_hook(e.mlx, ft_loop, &e);
		mlx_loop(e.mlx);
	}
	else
		ft_putendl("Usage: ./wolf3d <map>");
	return (0);
}
