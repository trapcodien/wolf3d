/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 06:37:17 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 03:30:59 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "wolf.h"

static void		ft_get_size_map(int fd, int *width, int *height)
{
	char	*line;
	char	*ptr;

	get_next_line(fd, &line);
	if ((ptr = ft_strchr(line, ';')))
	{
		*ptr = 0;
		ptr = ptr + 1;
	}
	else
	{
		free(line);
		ft_exit("map : bad format.");
	}
	if (ft_strisnum(line) && ft_strisnum(ptr))
	{
		*width = ft_atoi(line);
		*height = ft_atoi(ptr);
	}
	else
	{
		free(line);
		ft_exit("map : bad format.");
	}
	free(line);
}

static int		**ft_create_map(t_env *e, int fd)
{
	int		**map;
	int		i;
	int		x;
	int		y;

	ft_get_size_map(fd, &x, &y);
	e->mapx = x;
	e->mapy = y;
	map = (int **)malloc(sizeof(int *) * x);
	i = 0;
	while (i < x)
	{
		map[i] = (int *)malloc(sizeof(int) * y);
		i++;
	}
	return (map);
}

void			ft_check_map(t_env *e)
{
	int		i;

	i = 0;
	while (i < e->mapx)
	{
		if (e->map[i][e->mapy - 1] == 0 || e->map[i][0] == 0)
			ft_exit("map : bad format.");
		i++;
	}
	i = 0;
	while (i < e->mapy)
	{
		if (e->map[0][i] == 0 || e->map[e->mapx - 1][i] == 0)
			ft_exit("map : bad format.");
		i++;
	}
}

int				**ft_parse_map(t_env *e, char *path, int i, int j)
{
	int		**map;
	char	**split;
	char	*line;
	int		fd;

	if ((fd = open(path, O_RDWR)) == -1)
		ft_exit("map : bad file.");
	map = ft_create_map(e, fd);
	while ((get_next_line(fd, &line) == 1))
	{
		split = ft_strsplit(line, ' ');
		if (ft_split_len(split) != e->mapx || j > e->mapy)
			ft_exit("map : bad format.");
		i = 0;
		while (split[i] && i < e->mapx)
		{
			map[i][j] = ft_atoi(split[i]);
			i++;
		}
		ft_split_destroy(split);
		free(line);
		j++;
	}
	close(fd);
	return (map);
}

void			ft_destroy_map(int **map, int mapx)
{
	int		i;

	i = 0;
	while (i < mapx)
	{
		free(map[i]);
		i++;
	}
	free(map);
}
