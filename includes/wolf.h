/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/16 20:35:53 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 03:34:19 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <mlx.h>
# include "/opt/X11/include/X11/X.h"
# include "libft.h"
# include "struct.h"

# define WIN_WIDTH 1280
# define WIN_HEIGHT 800
# define WIN_TITLE "Wolf3D"

# define ABS(A) (((A) < 0) ? (-1 * (A)) : (A))
# define COLOR(R,G,B) ((R) * 65536 + (G) * 256 + (B))

# define KEY_ESC 65307
# define KEY_UP 65362
# define KEY_DOWN 65364
# define KEY_LEFT 65361
# define KEY_RIGHT 65363

# define UP_LEFT 1
# define UP_RIGHT 2
# define DOWN_LEFT 3
# define DOWN_RIGHT 4

# define NORTH COLOR(168, 168, 168)
# define SOUTH COLOR(163, 178, 242)
# define EAST COLOR(148, 148, 148)
# define WEST COLOR(123, 138, 202)
# define CEIL COLOR(42, 84, 255)
# define FLOOR COLOR(84, 84, 84)

# define ISWALL(c) (c == NORTH || c == SOUTH || c == EAST || c == WEST) ? 1 : 0

# define RSPEED 0.1

/*
** wolf.c
*/
int		ft_keypress(int keycode, t_env *e);
int		ft_keyrelease(int keycode, t_env *e);
int		ft_expose_hook(t_env *e);
int		ft_loop(t_env *e);

/*
** init.c
*/
void	init_player(t_env *e);
void	ft_exit(const char *str);

/*
** ft_image.c
*/
t_img	*ft_img_create(t_env *e, int w, int h);
void	ft_img_pixel_put(t_img *img, int x, int y, int color);
void	ft_img_reset(t_img *img);
void	ft_img_display(t_img *img);
void	ft_img_destroy(t_img *img);

/*
** ft_move.c
*/
void	move(int direction, t_env *e);
void	move_up(t_env *e);
void	move_down(t_env *e);
void	move_left(t_env *e);
void	move_right(t_env *e);

/*
** ft_calc.c
*/
void	ft_get_step(t_cast *rc);
void	ft_dda(t_cast *rc, t_env *e);
void	ft_init_raycast(t_cast *rc, t_env *e, int x);
void	ft_get_wdist(t_cast *rc);

/*
** ft_draw.c
*/
void	ft_put_slice(t_cast *rc, t_env *e, int x, t_uint color);
void	ft_draw(t_env *e);

/*
** ft_parser.c
*/
void	ft_check_map(t_env *e);
int		**ft_parse_map(t_env *e, char *path, int i, int j);
void	ft_destroy_map(int **map, int mapx);

#endif
