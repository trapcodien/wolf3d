/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 06:35:18 by garm              #+#    #+#             */
/*   Updated: 2014/11/06 02:51:56 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct			s_move
{
	int					up;
	int					down;
	int					left;
	int					right;
}						t_move;

typedef struct			s_player
{
	double				posx;
	double				posy;
	double				dirx;
	double				diry;
	double				planex;
	double				planey;
	t_move				move;
}						t_player;

typedef struct			s_env
{
	void				*mlx;
	void				*win;
	struct s_img		*screen;
	int					width;
	int					height;
	t_player			player;
	int					**map;
	int					mapx;
	int					mapy;
}						t_env;

typedef struct			s_img
{
	t_env				*e;
	void				*ptr;
	char				*data;
	int					bpp;
	int					sizel;
	int					endian;
	int					w;
	int					h;
}						t_img;

typedef struct			s_cast
{
	double				camx;
	double				rposx;
	double				rposy;
	double				rdirx;
	double				rdiry;
	int					mapx;
	int					mapy;
	double				sdistx;
	double				sdisty;
	double				ddistx;
	double				ddisty;
	double				wdist;
	int					stepx;
	int					stepy;
	int					side;
	int					hit;
}						t_cast;

typedef unsigned int	t_uint;

#endif
